﻿using UnityEngine;
using System.Collections;

public class mover : MonoBehaviour {


	public GameObject obj;
	Vector2 a, b;
	public float moveSpeed = 0.1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {




		a = obj.transform.position;
		b = Camera.main.ScreenToWorldPoint( Input.mousePosition);
		b = b - a;
		b.Normalize();
		b = b * moveSpeed + a;
		obj.transform.position = Vector3.Lerp( a, b, Time.deltaTime);
	
	}
}

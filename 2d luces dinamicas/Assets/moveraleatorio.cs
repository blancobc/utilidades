﻿using UnityEngine;
using System.Collections;

public class moveraleatorio : MonoBehaviour {


	public GameObject obj;
	public float limite = 5f;
	bool subo = true;

	public float speed = 0.1f;

	private Vector3 newPosition;    


	void Start () {newPosition = obj.transform.position;}

	void Update () {

		if(subo){
			newPosition.y = obj.transform.position.y + speed;
			if(newPosition.y > limite) subo=false;
		}
		else{
			newPosition.y = obj.transform.position.y - speed;
			if(newPosition.y < -limite) subo=true;
		}

		obj.transform.position = newPosition;   
	
	}
}

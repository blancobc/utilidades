﻿using UnityEngine;
using UnityEngine.SceneManagement;


// --------------------------
// Posibles estados de juego
// --------------------------
public enum TiposDeEstado { INTRO, MENU, JUEGO, SALIR }


// ------------------------
// Clase Game Manager
// ------------------------
public class GameManager : MonoBehaviour {
	
	// ------------------------
	// Atributos estaticos 
	// ------------------------

	//att privado (_instancia)
	static private GameManager _instancia;

	//att publico (instancia) por el que accedemos 
	static public GameManager instancia
	{
		// metodo get
		// se ejecuta al acceder por GameManager.instancia
		get
		{
			// si es la primera vez que accedemos a la instancia del GameManager, 
			// no existira, y la crearemos
			if (_instancia == null)
			{
				// creamos un nuevo objeto llamado "_MiGameManager"
				GameObject go = new GameObject("_MiGameManager");

				// anadimos el script "GameManager" al objeto
				go.AddComponent<GameManager>();

				// guardamos en la instancia el objeto creado
				// debemos guardar el componente ya que _instancia es del tipo GameManager
				_instancia = go.GetComponent<GameManager>();

				// hacemos que el objeto no se elimine al cambiar de escena
				DontDestroyOnLoad(go);
			}

			// devolvemos la instancia
			// si no existia, en este punto ya la habra creado
			return _instancia;
		}

		// metodo set
		// no implementado para no permitir modificar la instancia "GameManager.instancia = x;"
	}


	// Constructor
	// Lo ocultamos el constructor para no poder crear nuevos objetos "sin control"
	protected GameManager(){}


	// ------------
	// Atributos
	// ------------
	public int nivel;
	public int puntos;
	public int vida;
	public float valorTimeScale;

	// Control del estado
	public TiposDeEstado estadoActual;
	// Definimos previamente el enumerado
	// public enum TiposDeEstado { INTRO, MENU, JUEGO, SALIR }


	// ------------
	// Metodos
	// ------------
	public void cambiarEscena(string escena) { 
		guardarPartida();
		SceneManager.LoadScene(escena); 
	}

	public void reiniciar(){
		guardarPartida();
		//quitarPausa();
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}


	public void guardarPartida(){
		if(PlayerPrefs.GetInt ("nivelDesbloqueado") < nivel)
			PlayerPrefs.SetInt("nivelDesbloqueado", nivel);
		if(PlayerPrefs.GetInt ("puntosMax") < puntos) 
			PlayerPrefs.SetInt("puntosMax", puntos);
	}

	public void cargarPartida(){
		nivel = PlayerPrefs.GetInt("nivelDesbloqueado");
		puntos = PlayerPrefs.GetInt("puntosMax");
	}


	public void pausar(){
		// si esta activo lo pausamos
		if(Time.timeScale == 1){ Time.timeScale = 0; print("pausado");}
		// sino, quitamos la pausa
		else{ Time.timeScale = 1; print("despausado");}
	}


}
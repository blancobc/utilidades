using UnityEngine;

public class Intro : MonoBehaviour {

	// Al comenzar cambiamos al menu principal después de una espera
	void Start () {
		GameManager.instancia.estadoActual = TiposDeEstado.MENU;
		Invoke("aux", 3f);
	}

	public void aux(){
		GameManager.instancia.cambiarEscena("menu");
	}


}

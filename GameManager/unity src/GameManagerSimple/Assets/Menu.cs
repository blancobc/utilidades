﻿using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
	
	public Text puntosvalue;


	// Cargamos la partida
	void Start(){
		GameManager.instancia.cargarPartida();
		puntosvalue.text = GameManager.instancia.puntos.ToString();
	}


	// OPCIONES DEL MENU
	public void OpcionJuego(){
		GameManager.instancia.estadoActual = TiposDeEstado.JUEGO;
		GameManager.instancia.cambiarEscena("juego");
	}

	public void OpcionIntro(){
		GameManager.instancia.estadoActual = TiposDeEstado.INTRO;
		GameManager.instancia.cambiarEscena("intro");
	}

	public void OpcionSalir(){
		Debug.Log("Salimos");
		Application.Quit();
	}



}

﻿using UnityEngine;
using UnityEngine.UI;

public class Juego : MonoBehaviour {

	public Text puntosvalue;

	void Start () {
		GameManager.instancia.puntos = 0;
		puntosvalue.text = GameManager.instancia.puntos.ToString();
	}
	
	void Update () {
		if(Input.GetKey(KeyCode.Space)){
			GameManager.instancia.puntos++;
			puntosvalue.text = GameManager.instancia.puntos.ToString();
		}

		if(Input.GetKeyDown(KeyCode.Escape)){
			GameManager.instancia.pausar();
		}
	}


	// OPCIONES DEL MENU
	public void OpcionReiniciar(){
		GameManager.instancia.reiniciar();
	}

	public void OpcionPausa(){
		GameManager.instancia.pausar();
	}

	public void OpcionMenu(){
		GameManager.instancia.estadoActual = TiposDeEstado.MENU;
		GameManager.instancia.cambiarEscena("menu"); //este metodo del gamemanager ya guarda la partida
	}

}

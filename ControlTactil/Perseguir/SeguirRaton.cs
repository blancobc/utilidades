﻿using UnityEngine;
using System.Collections;

public class SeguirRaton : MonoBehaviour {


	private Vector3 a, b;
	public float velocidad = 10f;

	void Start () {

	}
	

	void Update () {

		//moverDirecto();
		//moverGradual();
		seguirRaton();
	}



	void moverDirecto(){
		a = transform.position;
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
			{
				b.x = hit.point.x;
				b.z = hit.point.z;
				//dejo la misma y ya que no quiero que mueva en altura
				b.y = transform.position.y;

				transform.position = b;
			}
		}
	}



	void moverGradual(){
		a = transform.position;

		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
			{
				b.x = hit.point.x;
				b.z = hit.point.z;
				//dejo la misma y ya que no quiero que mueva en altura
				b.y = transform.position.y;

				b = b - a;
				b.Normalize();
				b = b * velocidad + a;
				transform.position = Vector3.Lerp( a, b, Time.deltaTime);
			}
		}
	}
	


	void seguirRaton(){
		a = transform.position;
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit))
		{
			b.x = hit.point.x;
			b.z = hit.point.z;
			//dejo la misma y ya que no quiero que mueva en altura
			b.y = transform.position.y;

			b = b - a;
			b.Normalize();
			b = b * velocidad + a;
			transform.position = Vector3.Lerp( a, b, Time.deltaTime);
		}
	}

	
}

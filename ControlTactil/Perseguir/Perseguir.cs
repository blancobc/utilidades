﻿using UnityEngine;
using System.Collections;

public class Perseguir : MonoBehaviour {


	private Vector3 a, b;
	public float velocidad = 10f;
	public GameObject objetivo;

	void Start () {

	}
	

	void Update () {

		transform.position = Vector3.MoveTowards(transform.position, 
		                                         objetivo.transform.position, 
		                                         velocidad * Time.deltaTime);

		transform.LookAt (objetivo.transform);
	}

	
}

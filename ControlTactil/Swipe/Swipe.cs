﻿using UnityEngine;
using System.Collections;

public class Swipe : MonoBehaviour {


	private Vector2 dir = Vector2.zero;
	private Vector2 touchOrigin = -Vector2.one;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		// Control por teclado si ejecutamos en editor, escritorio o web
		
		#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
		
		float x = (int) (Input.GetAxisRaw ("Horizontal"));
		float y = (int) (Input.GetAxisRaw ("Vertical"));
		//Si nos movemos en horizontal marcamos la vertical a 0
		if(x != 0) y = 0;
		
		if (x > 0) dir = Vector2.right;
		else if (x < 0) dir = -Vector2.right;
		else if (y > 0) dir = Vector2.up;
		else if (y < 0) dir = -Vector2.up;
		
		
		
		// Control con gestos si ejecutamos en dispositivos moviles
		
		#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
		
		if (Input.touchCount > 0)
		{
			//Guardamos el primer toque
			Touch myTouch = Input.touches[0];
			
			//Si comienza un arrastre cogemos la posicion de comienzo
			if (myTouch.phase == TouchPhase.Began)
			{
				touchOrigin = myTouch.position;
			}
			
			//Si finaliza el arrastre
			else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0)
			{
				//Posicion de final del toque
				Vector2 touchEnd = myTouch.position;
				
				//Diferencia entre el principio y final del arrastre
				float x = touchEnd.x - touchOrigin.x;
				float y = touchEnd.y - touchOrigin.y;
				
				//No repetimos hasta que se origine un nuevo arrastre
				touchOrigin = -Vector2.one;
				
				//Si el arrastre ha sido muy corto (menos de 8 px)
				//ha sido una pulsacion que descartamos
				if(Mathf.Abs(x) > 8 || Mathf.Abs(y) > 8){ 
					//Obtenemos la direccion del arrastre
					if (Mathf.Abs(x) > Mathf.Abs(y)){
						if(x>0) dir = Vector2.right;
						else dir = -Vector2.right;
					}
					else{
						if(y>0) dir = Vector2.up;
						else dir = -Vector2.up;
					}
				}
				
			}
			
		}
		
		#endif



		// Movemos el objeto
		transform.Translate (dir.x * Time.deltaTime, 
		                     0, 
		                     dir.y * Time.deltaTime);


	}
}

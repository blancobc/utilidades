﻿using UnityEngine;
using System.Collections;

public class GeneradorNiveles : MonoBehaviour {



	public TextAsset[] nivelesCSV;
	public int xMax = 10;
	public int yMax = 10;

	// -1 o 0.Nada, 1.PJ,... 
	public GameObject[] prefabs;




	private int nivelActual = -1;
	private int[][] nivel = new int[][]{
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}
	};


	void Start () {

		generarSiguienteNivel();

	}




	public void generarSiguienteNivel(){

		// genera un nivel nuevo distinto al actual
		int numeroDeNivel;
		do{
			numeroDeNivel = Random.Range(0, nivelesCSV.Length);
		}while(numeroDeNivel == nivelActual);

		borrarNivel();
		cargarCSV(nivelesCSV[numeroDeNivel]);
		dibujarNivel();
		nivelActual = numeroDeNivel;
	}


	public void borrarNivel(){
		// reseteo la matriz
		for(int i=0; i < xMax; i++)
			for(int j=0; j< yMax; j++)
				nivel[i][j]=-1;

		// elimino los gameobjects de la jerarquia
		GameObject go = GameObject.Find("Nivel");
		if(go != null){
			Destroy(go);
		}
	}



	public void cargarCSV(TextAsset fichero) {

		char separadorLinea = '\n';
		char separadorCampo = ',';

		// Proceso el fichero. Traigo todas las lineas
		string[] lineas = fichero.text.Split (separadorLinea);
		string[] campos;

		// Las recorro campo a campo
		for(int fila = 0; fila < lineas.Length-1; fila++){
	
			campos = lineas[fila].Split(separadorCampo);
			for(int col = 0; col < campos.Length; col++){
				nivel[fila][col] = int.Parse (campos[col]);
			}
		}
	}



	public void dibujarNivel(){

		// por organizacion, todos los objetos generados los metere anidados en un padre
		GameObject padre = new GameObject("Nivel");

		// recorro la informacion del nivel y voy creando los objetos
		for(int x=0; x<nivel.Length; x++){
			for(int y=0; y<nivel[x].Length; y++){

				int n = nivel[x][y];

				// si es -1 no pinto nada y sino, su prefab correspondiente
				if(n >= 0 && n < prefabs.Length){
					GameObject nuevo = (GameObject) Instantiate(prefabs[n], new Vector2(y, nivel.Length - x), Quaternion.identity);
					nuevo.transform.parent = padre.transform;
				}
			}
		}
	}
		

}

﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
	public static GameController _instance;

	public Transform wall;
	public Transform player;
	public Transform orb;
	public Transform goal;

	public GUIText scoreText;

	private int orbsCollected;
	private int orbsTotal;

	private ParticleSystem goalPS;

	public ParticleSystem GoalPS
	{
		get
		{
			return goalPS;
		}

		set
		{
			goalPS = value;
		}
	}

	public void CollectedOrb()
	{
		orbsCollected++;
		scoreText.text = "Orbs: " + orbsCollected + "/" + orbsTotal;

		if(orbsCollected >= orbsTotal)
		{
			goalPS.Play();
		}
	}



	void Awake()
	{
		_instance = this;
	}

	// Use this for initialization
	void Start() 
	{
		GameObject[] orbs;
		orbs = GameObject.FindGameObjectsWithTag("Orb");
		
		orbsCollected = 0;
		orbsTotal = orbs.Length;

		scoreText.text = "Orbs: " + orbsCollected + "/" + orbsTotal;
	}

	void Update()
	{
		if(Input.GetKeyDown("f2"))
		{
			this.gameObject.GetComponent<LevelEditor>().enabled = true;
		}
		
	}

	public void UpdateOrbTotals(bool reset = false)
	{
		if (reset)
			orbsCollected = 0;
		
		GameObject[] orbs;
		orbs = GameObject.FindGameObjectsWithTag("Orb");
		
		orbsTotal = orbs.Length;
		
		scoreText.text = "Orbs: " + orbsCollected + "/" + orbsTotal;
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Xml;
using System.IO;

public class procesaXML : MonoBehaviour {

	public TextAsset ficheroDatos;
	public GameObject prefabSuceso;
	public GameObject prefabActividad;
	private GameObject go;
	

	void Start () {

		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.LoadXml(ficheroDatos.text);
		XmlNodeList nodos = null;
		int id, origen, destino;
		float x, y, comp;
		string idstr;


		//SUCESOS
		nodos = xmlDoc.GetElementsByTagName("suceso");

		//recorro
		for(int i=0; i<nodos.Count; i++){

			//extraigo info del xml
			id = int.Parse(nodos[i].Attributes["id"].Value);
			x = float.Parse(nodos[i].SelectSingleNode("x").InnerText);
			y = float.Parse(nodos[i].SelectSingleNode("y").InnerText);
			comp = float.Parse(nodos[i].SelectSingleNode("comp").InnerText);

			//almaceno
			go = (GameObject) Instantiate(prefabSuceso, new Vector2(x,y), Quaternion.identity);
			go.GetComponent<Suceso>().asigna (id, new Vector2(x,y),comp);	
		}


		//ACTIVIDADES
		nodos = xmlDoc.GetElementsByTagName("actividad");

		//recorro
		for(int i=0; i<nodos.Count; i++){
			
			//extraigo info del xml
			idstr = nodos[i].Attributes["id"].Value;
			origen = int.Parse(nodos[i].SelectSingleNode("origen").InnerText);
			destino = int.Parse(nodos[i].SelectSingleNode("destino").InnerText);
			comp = float.Parse(nodos[i].SelectSingleNode("comp").InnerText);

			//almaceno
			go = (GameObject)Instantiate (prefabActividad);
			go.GetComponent<Actividad>().asigna (idstr,
			                                     GameObject.Find("s"+origen.ToString()),
			                                     GameObject.Find("s"+destino.ToString()),
			                                     comp);

		}

		 
	}
	


}

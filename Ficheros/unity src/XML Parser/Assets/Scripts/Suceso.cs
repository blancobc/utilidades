using UnityEngine;
using System;

public class Suceso : MonoBehaviour {

	public int id;
	public Vector2 posicion;
	public float porcentajecompletado;	
	public SpriteRenderer sr;


	public void asigna (int id, Vector2 posicion, float comp)
	{
		this.name = "s" + id.ToString();
		this.id = id;
		this.posicion = posicion;
		this.porcentajecompletado = comp;
		this.actualiza(comp);
	}
	

	public void actualiza(float pc){
		this.porcentajecompletado = pc;
		this.sr = this.GetComponent<SpriteRenderer>();
		this.transform.localScale = new Vector3(pc/100, pc/100, 0);
		sr.color = Color.Lerp(Color.black, Color.white, pc/100);


	}

}
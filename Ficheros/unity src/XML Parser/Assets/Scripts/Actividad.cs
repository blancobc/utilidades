﻿using UnityEngine;
using System.Collections;

public class Actividad : MonoBehaviour {

	public string id;
	public GameObject origen, destino;
	public float porcentajecompletado;	
	public LineRenderer [] lr;

	public void asigna (string id, GameObject o, GameObject d, float comp)
	{
		this.name = "a" + id.ToString();
		this.id = id;
		this.origen = o;
		this.destino = d;
		this.actualiza (comp);
	}
	

	public void actualiza(float pc){

		this.porcentajecompletado = pc;
		Color c = Color.Lerp(Color.red, Color.green, pc/100);

		this.lr = this.GetComponentsInChildren <LineRenderer>();

		//linea base
		lr[0].SetPosition (0, origen.transform.position);
		lr[0].SetPosition (1, destino.transform.position);
		lr[0].SetColors (c,c);

		//linea de progreso
		lr[1].SetPosition (0, origen.transform.position);
		lr[1].SetPosition (1, Vector2.Lerp(
										origen.transform.position,
										destino.transform.position,
										pc/100)
		                       			);
		lr[1].SetColors (c,c);
	}

}

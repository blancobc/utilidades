﻿using UnityEngine;
using System.Collections;

public class Camara : MonoBehaviour {


	public float velocidadCamara = 10f;
	public int id = -1;
	GameObject g;


	// Controles para cambio de camara (pan y zoom)
	void Update () {
		if(Input.GetKey("right")) transform.Translate (new Vector2(velocidadCamara * Time.deltaTime, 0));
		if(Input.GetKey("left")) transform.Translate (new Vector2(velocidadCamara * Time.deltaTime * -1, 0));
		if(Input.GetKey("up")) transform.Translate (new Vector2(0, velocidadCamara * Time.deltaTime));
		if(Input.GetKey("down")) transform.Translate (new Vector2(0, velocidadCamara * Time.deltaTime * -1));
		if(Input.GetKey("z")) Camera.main.orthographicSize += velocidadCamara * Time.deltaTime;
		if(Input.GetKey("x")) Camera.main.orthographicSize -= velocidadCamara * Time.deltaTime;
	}

	// Al pulsar sobre un nodo (suceso) muestra su informacion
	void FixedUpdate() {
		
		if(Input.GetButton ("Fire1")){
			
			Vector2 posicionRaton = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast(posicionRaton, -Vector2.up);
				
			if (hit.collider != null) {
				if(hit.collider.tag == "sucesos"){
					id = hit.collider.GetComponent<Suceso>().id;
				}
			}
		}
	}

	// Muestro la informacion del suceso seleccionado actualmente
	void OnGUI() {

		g = GameObject.Find("s"+id.ToString());

		if(g){
		
			string info = string.Format(
				"Suceso: {0}\nCompletado: {1} %",
				g.GetComponent<Suceso>().id,
				g.GetComponent<Suceso>().porcentajecompletado
			);

			GUI.Label(new Rect(10, 50, 1000, 1000), info);
		}
	}

	
}

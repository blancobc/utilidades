﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class CSVParser : MonoBehaviour {


	public TextAsset ficheroCSV; 

	public InputField id, nombre, salud, destreza, magia, fortaleza;
	public Text textoRegistros;

	private char separadorLinea = '\n';
	private char separadorCampo = ','; 


	void Start ()
	{
		leerFichero ();
	}


	private void leerFichero()
	{

		// Proceso el fichero. Traigo todas las lineas
		string[] lineas = ficheroCSV.text.Split (separadorLinea);
		string[] campos;

		// Las recorro campo a campo
		textoRegistros.text = "";
		for(int fila = 0; fila < lineas.Length; fila++){
			campos = lineas[fila].Split(separadorCampo);
			for(int col = 0; col < campos.Length; col++){
				textoRegistros.text += campos[col] + " ";
			}
			textoRegistros.text += "\n";			
		}
	}


	/* recorrido usando foreach
			foreach (string linea in lineas)
			{
			string[] campos = linea.Split(separadorCampo);
				foreach(string campo in campos)
				{
					textoRegistros.text += campo + "\t";
				}
				textoRegistros.text += '\n';
			}
	*/



	public void escribirFichero()
	{
		// anadimos los registros actuales al fichero
		File.AppendAllText(getPath() + "/" + ficheroCSV.name + ".csv",
			separadorLinea + 
			id.text + separadorCampo + nombre.text + separadorCampo + 
			salud.text + separadorCampo + destreza.text + separadorCampo + magia.text + separadorCampo + fortaleza.text);

		// refrescamos los datos
		id.text=""; nombre.text=""; salud.text=""; destreza.text=""; magia.text=""; fortaleza.text="";

		#if UNITY_EDITOR
			UnityEditor.AssetDatabase.Refresh ();
		#endif

		leerFichero();
	}


	// obtener el path según el entorno de ejecucion
	private static string getPath(){

		#if UNITY_EDITOR
		return Application.dataPath;
		#elif UNITY_ANDROID
		return Application.persistentDataPath;// +fileName;
		#elif UNITY_IPHONE
		return GetiPhoneDocumentsPath();// +"/"+fileName;
		#else
		return Application.dataPath;// +"/"+ fileName;
		#endif
	}
	// obtener el path en ios
	private static string GetiPhoneDocumentsPath(){
		string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
		path = path.Substring(0, path.LastIndexOf('/'));
		return path + "/Documents";
	}


}